# XP-Pen Tablet

This driver enables using official XP-Pen Graphic Tablets.
Tested on XP-Pen Deco 01 v2.

This is an upgrade on the https://aur.archlinux.org/packages/xp-pen-tablet-beta-driver
driver from Poilrouge.
